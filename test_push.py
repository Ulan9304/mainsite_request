import unittest
from pprint import pprint

import requests

from constants.constants import token
from urls.urls import push, push_send

class TestCase(unittest.TestCase):
    """Push notification endpoint"""

    def setUp(self):
        self.headers = {"Authorization": token}

    def test_push(self):
        """Create or update Firebase  fcm id"""
        response = requests.post(push,
        json = {
            "account": "996776514377",
            "fcmId": "1"
        },
        headers = self.headers)
        # print (response.json())
        self.assertTrue(200 <= response.status_code <= 400,response.status_code), "FAILED"

    def test_push_send(self):
        """Send push notification"""
        response = requests.post(push_send,
        json = {
            "data": {
                    "additionalProp1": "string",
                    "additionalProp2": "string",
                    "additionalProp3": "string"
                    },
                "notification": {
                    "body": "string",
                    "sound": "string",
                    "title": "string"
                                },
                    "priority": "string",
                    "target": [
                    "string"
                        ],
                    "targetType": "string"
                    },
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 400,response.status_code), "FAILED"

        
def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())