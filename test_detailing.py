import unittest
from pprint import pprint

import requests

from constants.constants import token

from datetime import datetime, timedelta, timezone
from urls.urls import detailing, detailing_via_email, detailing_via_email_by_period_type, detailing_periods


def add_days(date, amount):
    return date + timedelta(days=amount)

def get_now():
    return datetime.utcnow()

def get_tomorrow(date_from=get_now()):
    return add_days(date_from, 1)



class TestCase(unittest.TestCase):
    """Detailing endpoint"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_detailing(self):
        """Get details by period"""
        now_date = get_now()
        params = {"period": now_date.isoformat()}
        response = requests.get(detailing,
            params = params,
            headers = self.headers)
        # pprint (response.text)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_detailing_via_email_by_period(self):
        """ Get details via email"""
        now_date = get_now()
        tomorrow = get_tomorrow()
        response = requests.post(detailing_via_email,
            json = {"email": "miyavi@gmail.com",
                    "period": now_date.isoformat(),
                    "periodEnd": tomorrow.isoformat(),
                    "periodStart": now_date.isoformat()       
        },
        headers = self.headers)
        #response.status_code
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_detailing_via_email_by_period_type(self):
        """ Get details via email by period type """
        now_date = get_now()
        tomorrow = get_tomorrow()
        response = requests.post(detailing_via_email_by_period_type,
            json = {"email": "miyavi@gmail.com",
                    "period": now_date.isoformat(),
                    "periodEnd": tomorrow.isoformat(),
                    "periodStart": now_date.isoformat()       
        },
        headers = self.headers
        )
        #response.status_code
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_detailing_periods(self):
        """ Get periods"""
        response = requests.get(detailing_periods,
        headers = self.headers)
        #response.status_code
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"   


def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite    

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
