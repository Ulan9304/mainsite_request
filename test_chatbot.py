import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import chatbot, chatbot_catalog

class TestCase(unittest.TestCase):
    """Chatbot endpoint"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_chatbot(self):
        response = requests.get(chatbot,
        headers = self.headers)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_chatbot_catalog(self):
        response = requests.get(chatbot_catalog,
        headers = self.headers)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
