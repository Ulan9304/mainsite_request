from constants.constants import BASE_URL, serviceId, account, subAccount, offerId

alias = "null"
orderId = "27652"
fcmId = "null"
# пакет 8 гб
# Dashboard_urls

dashboard_subscription = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/dashboard"

dashboard_service = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/dashboard/services"

dashboard_subscription_service = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/dashboard/services/{serviceId}"

dashboard_subscription_services_list = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/services"

dashboard_subscription_service_ids = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/services_ids"

dashboard_user_info = BASE_URL + "/telco/user-info"



# Detailing url 

detailing = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/details"

detailing_via_email = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/details/via-email/by-period"

detailing_via_email_by_period_type = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/details/via-email/by-period-type"

detailing_periods = BASE_URL + "/telco/details/periods"


# Offers url

offer_bls_accept = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/bls/offer/accept"

# getBlsOfferDetails
offer_bls_offer_details = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/bls/offer/details"

# getMainBlsOffer
offer_bls_offer_main = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/bls/offer/main"

# getBlsOffers
offer_get_bls_offers = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/bls/offers"

# Get offer categories
offer_bls_offercategories = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/offercategories"

# Get offers by category Id
offer_bls_offercategories_alias = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/offercategories/{alias}"

# Get list of offers (all fields)
offer_offers = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/offers"

# getlistofOffers
offer_bls_offers_basic = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/offers-basic"

# getOfferbyid
offer_bls_offers_offerId = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/offers/{offerId}"



# Orders

get_orders = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/orders"

# Create new order
post_orders = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/orders"

get_orderId = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/orders/{orderId}"

put_orderId = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/orders/{orderId}"



# PUSH

push = BASE_URL + "/telco/fcm"
# Create or update Firebase fcm Id

push_send = BASE_URL + "/telco/push/send"


# USER

user = BASE_URL + f"/telco/{account}"

user_password_change = BASE_URL + f"/telco/{account}/password/change"

user_exist = BASE_URL + f"/telco/{account}/password/exists"

user_settings = BASE_URL + f"/telco/{account}/settings"

user_settings_change = BASE_URL + f"/telco/{account}/settings/change"

user_fill_empty_names = BASE_URL + f"/telco/fill-empty-names"

# Grafana

grafana = BASE_URL + "/telco/grafana/item"

# FAQ

faq = BASE_URL + "/telco/faq/mybeeline"

# HIPPO

hippo_events = BASE_URL + "/telco/events"
hippo_basic = BASE_URL + "/telco/events-basic"
hippo_event_alias = BASE_URL + f"/telco/events/{alias}"


# Chatbot

chatbot = BASE_URL + f"/telco/{account}/subscriptions/{subAccount}/chatbotdoc"
chatbot_catalog = BASE_URL + "/telco/chatbotdoc/catalog"