import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import put_orderId, get_orderId, get_orders, post_orders


class TestCase(unittest.TestCase):
    """Orders endpoint"""

    def setUp(self):
        self.headers = {"Authorization": token}

    def test_get_orders(self):
        """Get orders"""
        response = requests.get(get_orders,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_post_orders(self):
        """Create new order"""
        response = requests.post(post_orders,
        params = {
            "action": "Add",
            "offerId": "A128438",
            "serviceId": "A128349"
        },
        headers = self.headers)
        # pprint (response)
        self.assertTrue(200 <= response.status_code <= 400,response.status_code), "FAILED"


    def test_get_orderId(self):
        """Get order by id"""
        response = requests.get(get_orderId,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_put_orderId(self):
        """Execute order"""
        response = requests.put(put_orderId,
        headers = self.headers)
        # pprint (response)
        self.assertTrue(200 <= response.status_code <= 400,response.status_code), "FAILED"

 
def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
