import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import hippo_events, hippo_basic, hippo_event_alias

class TestCase(unittest.TestCase):
    """Events endpoint"""

    def setUp(self):
        self.headers = {"Authorization": token}

    def test_hippo(self):
        """Get all event news"""
        params = {"lang": "RU",
                  "pageNo": 1,
                  "pageSize": 1,
                  "type": "type"}
        response = requests.get(hippo_events, params = params,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_hippo_basic(self):
        """Get all event news with basic fields"""
        params =  {"lang": "RU",
                  "pageNo": 1,
                  "pageSize": 1,
                  "type": "type"}
        response = requests.get(hippo_events, params = params,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_hippo_event_alias(self):
        """Get all event alias"""
        params =  {"lang": "RU",}
        response = requests.get(hippo_event_alias, params = params,
        headers = self.headers)
        # pprint (response.text)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"



def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
