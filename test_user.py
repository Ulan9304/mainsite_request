import unittest
from unittest.mock import patch, Mock
from pprint import pprint

import requests

from constants.constants import token
from urls.urls import user, user_exist, user_settings, user_fill_empty_names

class TestCase(unittest.TestCase):
    def setUp(self):
        """User service endpoint"""
        self.headers = {"Authorization": token}


    def test_user(self):
        """ Get user by account"""
        response = requests.get(user,
        headers = self.headers)
        # print (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_user_exist(self):
        """ Check if password exist"""
        response = requests.get(user_exist,
        headers = self.headers)
        # print (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_user_settings(self):
        """Get user settings"""
        response = requests.get(user_settings,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_user_fill_empty_names(self):
        """Fill empty names"""
        response = requests.get(user_fill_empty_names,
        headers = self.headers)
        # pprint (response.text)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    # @patch('main.test_user.test_user_password_change')
    # def test_user_password_change(self, MockChangePass):
    #     # """Change user password"""
    #     password = MockBlog()
    #     password.posts.return_value = [
    #         {
    #             "code" : "200",
    #             'status': "password changed"
    #         }
    #     ]
    #     response = password.posts()
    #     self.assertIsNotNone(response)
    #     self.assertIsInstance(response[0], dict)


def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())