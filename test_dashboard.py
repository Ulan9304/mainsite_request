import cutest as unittest
from pprint import pprint

import requests

from constants.constants import token
from urls.urls import (dashboard_service, dashboard_subscription,
                      dashboard_subscription_service,
                      dashboard_subscription_service_ids, dashboard_subscription_services_list,
                      dashboard_user_info)


class TestCase(unittest.TestCase):
    """A Dashboard. Generates dashboard for subscriber"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_dashboard_sub(self):
        """Generates main dashboard for subscriber"""
        response = requests.get(dashboard_subscription,
            headers = self.headers)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_dashboard_service(self):
        """Services in DashboardDto object"""
        response = requests.get(dashboard_service, 
            headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_dashboard_subscription_service(self):
        """Exactly service by ID in DashboardDto"""
        response = requests.get(dashboard_subscription_service,
            headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_dashboard_subscription_services_list(self):
        """List of services for specify customer."""
        response = requests.get(dashboard_subscription_services_list,
            headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_dashboard_subscription_service_ids(self):
        """List of services IDs for specify customer."""
        response = requests.get(dashboard_subscription_service_ids,
            headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_dashboard_user_info(self):
        """User information"""
        params = {
            "account": "996776514377",
            "subAccount": "996776514377"
        }
        response = requests.get(dashboard_user_info, params = params,
        headers = self.headers)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
