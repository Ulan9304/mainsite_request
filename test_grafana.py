import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import grafana

class TestCase(unittest.TestCase):
    """Grafana endpoint"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_grafana(self):
        """Grafana item"""
        response = requests.get(grafana,
        headers = self.headers)
        # pprint (response.text)
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
