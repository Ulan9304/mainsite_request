import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import (
  offer_bls_offer_main, offer_get_bls_offers, offer_bls_offercategories,
  offer_bls_offercategories_alias, offer_offers,
  offer_bls_offers_basic,offer_bls_offers_offerId)


class TestCase(unittest.TestCase):
    """Offers endpoint for BLS"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_offer_bls_offer_main(self):
        """Get main bls offers"""
        response = requests.get(offer_bls_offer_main,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_offers_get_bls_offers(self):
        """Get bls offers"""
        response = requests.get(offer_get_bls_offers,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


    def test_offers_bls_offer_categories(self):
        """ Get offer categories"""
        response = requests.get(offer_bls_offercategories,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_offer_bls_offercategories_alias(self):
        """Get offer by category id"""
        response = requests.get(offer_bls_offercategories_alias,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_offers_bls_offers(self):
        """Get list of offers (all fields)"""
        response = requests.get(offer_offers,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_offer_bls_offers_basic(self):
        """Get list of offers (basic fields)"""
        response = requests.get(offer_bls_offers_basic,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    def test_offer_bls_offers_offerId(self):
        """Get offer by id"""
        response = requests.get(offer_bls_offers_offerId,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())