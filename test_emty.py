import unittest
from pprint import pprint

import json
import requests

from constants.constants import token, offerId

from urls.urls import offer_bls_accept, offer_bls_offer_details, offer_bls_offer_main


class TestCase(unittest.TestCase):
    """Test where result we dont know"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_offers_bls_accept(self):
        response = requests.get(offer_bls_accept,
        params = {
            "offerId": offerId
        },
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"

    
    def test_offer_bls_offer_details(self):
        response = requests.get(offer_bls_offer_details,
            json = offerId,
            headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"
        

    def test_offers_bls_offer_main(self):
        response = requests.get(offer_bls_offer_main,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"



def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())