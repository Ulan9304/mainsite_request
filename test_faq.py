import unittest
from pprint import pprint

# import json
import requests

from constants.constants import token

from urls.urls import faq

class TestCase(unittest.TestCase):
    """FAQ endpoint"""
    def setUp(self):
        self.headers = {"Authorization": token}

    def test_faq(self):
        """Get My Beeline FAQ"""
        response = requests.get(faq,
        headers = self.headers)
        # pprint (response.json())
        self.assertTrue(200 <= response.status_code <= 300,response.status_code), "FAILED"


def suite():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestCase)
    return suite

if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())
